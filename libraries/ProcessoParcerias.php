<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/Dao.php';

class ProcessoParcerias extends Dao{

    function __construct(){
        parent::__construct('processo_parcerias');
    }    

    public function insert($data, $table = null) {
        // mais uma camada de segurança... além da validação
        $cols = array('conteudo');
        $this->expected_cols($cols);

        return parent::insert($data);
    }
}