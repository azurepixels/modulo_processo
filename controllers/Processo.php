<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Processo extends MY_Controller{

    public function index(){
        $data['ativo'] = 'prop';
        $data['conteudo'] = '<h3>Propósito</h3>
                            <a href="'.base_url('processo/criar_proposito').'"><button type="button" class="btn btn-primary">Criar</button></a>';
        $html = $this->load->view('common/header', '', true);
        $html .= $this->load->view('navtab', $data, true);
        $html .= $this->load->view('common/footer', '', true);
        $html .= $this->load->view('common/page_footer', '', true);
        $this->show($html);
    }

    public function processos(){
        $data['ativo'] = 'proc';
        $data['conteudo'] = '<h3>Processos</h3>';
        $html = $this->load->view('common/header', '', true);
        $html .= $this->load->view('navtab', $data, true);
        $html .= $this->load->view('common/footer', '', true);
        $html .= $this->load->view('common/page_footer', '', true);
        $this->show($html);
    }

    public function pessoas(){
        $data['ativo'] = 'pess';
        $data['conteudo'] = '<h3>Pessoas</h3>';
        $html = $this->load->view('common/header', '', true);
        $html .= $this->load->view('navtab', $data, true);
        $html .= $this->load->view('common/footer', '', true);
        $html .= $this->load->view('common/page_footer', '', true);
        $this->show($html);
    }

    public function parcerias(){
        $data['ativo'] = 'parc';
        $data['conteudo'] = '<h3>Parcerias</h3>';
        $html = $this->load->view('common/header', '', true);
        $html .= $this->load->view('navtab', $data, true);
        $html .= $this->load->view('common/footer', '', true);
        $html .= $this->load->view('common/page_footer', '', true);
        $this->show($html);
    }

}