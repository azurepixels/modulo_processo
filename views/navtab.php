<div class="container">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link <?php if($ativo == 'prop') echo "active disabled"; ?>" href="<?= base_url('processo') ?>">Propósito</a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?php if($ativo == 'proc') echo "active disabled"; ?>" href="<?= base_url('processo/processos') ?>">Processos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?php if($ativo == 'pess') echo "active disabled"; ?>" href="<?= base_url('processo/pessoas') ?>">Pessoas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?php if($ativo == 'parc') echo "active disabled"; ?>" href="<?= base_url('processo/parcerias') ?>">Parcerias</a>
        </li>
    </ul>
    <?= $conteudo ?>
</div>