
--
-- Database: `lp2_modulo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `processo_proposito`
--

CREATE TABLE `processo_proposito` (
  `id` int(11) NOT NULL,
  `conteudo` varchar(512) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `processo_proposito`
--
ALTER TABLE `processo_proposito`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `processo_proposito`
--
ALTER TABLE `processo_proposito`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `processo_processo`
--

CREATE TABLE `processo_processo` (
  `id` int(11) NOT NULL,
  `conteudo` varchar(512) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `processo_proposito`
--
ALTER TABLE `processo_processo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `processo_proposito`
--
ALTER TABLE `processo_processo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;
-- --------------------------------------------------------

--
-- Estrutura da tabela `processo_proposito`
--

CREATE TABLE `processo_pessoas` (
  `id` int(11) NOT NULL,
  `conteudo` varchar(512) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `processo_proposito`
--
ALTER TABLE `processo_pessoas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `processo_proposito`
--
ALTER TABLE `processo_pessoas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;
-- --------------------------------------------------------

--
-- Estrutura da tabela `processo_proposito`
--

CREATE TABLE `processo_parcerias` (
  `id` int(11) NOT NULL,
  `conteudo` varchar(512) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `processo_proposito`
--
ALTER TABLE `processo_parcerias`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `processo_proposito`
--
ALTER TABLE `processo_parcerias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;